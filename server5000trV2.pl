#!/usr/bin/perl

use strict;

use threads;

use IO::Socket;

use server;

sub process_connection {
    my ($sock, $addr) = @_;

    server::process_connection($sock);

}

    my $proto = getprotobyname('tcp');
    # create a socket, make it reusable
    socket(SOCKET, PF_INET, SOCK_STREAM, $proto)
       or die "Can't open socket $!\n";
    setsockopt(SOCKET, SOL_SOCKET, SO_REUSEADDR, 1)
       or die "Can't set socket option to SO_REUSEADDR $!\n";

    # bind to a port, then listen
    bind( SOCKET, pack_sockaddr_in(5007, inet_aton("127.0.0.1")))
       or die "Can't bind to ! \n";

    listen(SOCKET, 5) or die "listen: $!";
    print "SERVER started on port 5007\n";

sub worker {
    my ($n, $SOCKET) = @_;

    while (my $addr = accept(NEW_SOCKET, $SOCKET)) 
    {
         process_connection(*NEW_SOCKET, $addr);
    }


}

for my $n (1..20) {
    my $thr = threads->create(\&worker, $n, *SOCKET);
}

foreach my $thr (threads->list) {
    $thr->join;
}
