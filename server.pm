#------------------------------------------------------
=head1 NAME

=head1 SYNOPSIS

=cut
#------------------------------------------------------
use Time::Piece;

    use strict;

    package server;
#------------------------------------------------------
# null
#------------------------------------------------------
sub null {
    my ($par)=@_;

    return $par;
}
#------------------------------------------------------
# process_connection
#------------------------------------------------------
sub process_connection {
     my ($sock) = @_;

     select($sock);
     $|++;

     print STDOUT "conected\n";
     my $printed=0;
     my $printed2=0;
     while(1)
     {

        my $t  = Time::Piece->strptime(time(), '%s');
        my $stat=print sprintf("1\t%s\n",$t->strftime("%m%d%Y%H%M%S"));

        last unless $stat;
        
#        last;
        sleep(1);
     }
     print STDOUT "closed\n";
     close($sock);

     return;
}
#------------------------------------------------------
1;
